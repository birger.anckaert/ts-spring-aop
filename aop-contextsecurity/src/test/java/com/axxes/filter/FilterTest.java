package com.axxes.filter;

import com.axxes.aspect.AccountFilterAspect;
import com.axxes.domain.Account;
import com.axxes.domain.Customer;
import com.axxes.repository.AccountRepository;
import configuration.SystemConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = SystemConfiguration.class)
public class FilterTest {

    @Autowired
    private AccountFilterAspect accountFilterAspect;

    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    public void setUp() {
        accountFilterAspect.clearCurrentCustomer();
    }

    @Test
    public void springBeanFiltersEverythingIfNoCustomerSet() {
        assertNull(accountRepository.getAccount(42));
        assertNull(accountRepository.getAccount(1));
    }

    @Test
    public void springBeanFiltersOtherAccountIfCustomerSet() {
        accountFilterAspect.setCurrentCustomer(new Customer("John", "Doe"));
        assertEquals(accountRepository.getAccount(42), new Account("John", "Doe", 42));
        assertNull(accountRepository.getAccount(1));
    }

    @Test
    public void plainObjectWontFilter() {
        AccountRepository plainAccountRepository = new AccountRepository();
        assertEquals(plainAccountRepository.getAccount(42), new Account("John", "Doe", 42));
        assertEquals(plainAccountRepository.getAccount(1), new Account("Jane", "Doe", 1));
    }

}
