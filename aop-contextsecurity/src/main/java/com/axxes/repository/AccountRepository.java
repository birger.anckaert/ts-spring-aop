package com.axxes.repository;

import com.axxes.domain.Account;
import org.springframework.stereotype.Repository;

@Repository
public class AccountRepository {

    public Account getAccount(int id) {
        if (id == 42) {
            return new Account("John", "Doe", 42);
        } else {
            return new Account("Jane", "Doe", id);
        }
    }

}
