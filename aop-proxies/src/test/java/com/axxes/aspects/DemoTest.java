package com.axxes.aspects;

import com.axxes.demo.DemoClass;
import configuration.SystemConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = SystemConfiguration.class)
public class DemoTest {

    @Autowired
    DemoAspect demoAspect;

    @Autowired
    DemoClass demoClass;

    @BeforeEach
    public void setUp() {
        demoAspect.resetCalled();
    }

    @Test
    public void directCallToAdvicedMethodIsTraced() {
        assertFalse(demoAspect.isCalled());
        demoClass.advicedMethod();
        assertTrue(demoAspect.isCalled());
    }

    @Test
    public void indirectCallToAdvicedMethodIsNotTraced() {
        assertFalse(demoAspect.isCalled());
        demoClass.callsTheAdvicedMethod();
        assertFalse(demoAspect.isCalled());
    }

}
