package simpleaspect;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/simpleaspect.xml")
public class SimpleAspectXMLTest {

    @Autowired
    TracingAspect tracingAspect;

    @Autowired
    SomeService someService;

    @Test
    public void aspectIsCalled() {
        assertFalse(tracingAspect.isEnteringCalled());
        someService.someXMLConfigMethod(42);
        assertTrue(tracingAspect.isEnteringCalled());
    }

}
