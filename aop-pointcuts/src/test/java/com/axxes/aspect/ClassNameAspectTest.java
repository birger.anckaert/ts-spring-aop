package com.axxes.aspect;

import com.axxes.configuration.SystemConfiguration;
import com.axxes.repository.SimpleRepository;
import com.axxes.service.SimpleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = SystemConfiguration.class)
public class ClassNameAspectTest {

    @Autowired
    ClassNameAspect aroundTracingAspect;

    @Autowired
    SimpleService simpleService;

    @Autowired
    SimpleRepository simpleRepository;

    @BeforeEach
    public void setUp() {
        aroundTracingAspect.resetCalled();
    }

    @Test
    public void tracingOnServiceIsCalled() {
        assertEquals(aroundTracingAspect.getCalled(), 0);
        simpleService.doSomething();
        assertEquals(aroundTracingAspect.getCalled(), 1);
    }

    @Test
    public void tracingOnRepsositoryIsNotCalled() {
        assertEquals(aroundTracingAspect.getCalled(), 0);
        simpleRepository.doSomething();
        assertEquals(aroundTracingAspect.getCalled(), 0);
    }

}
